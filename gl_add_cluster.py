import argparse
import sys
import os
import boto3
from google.cloud import container_v1
import hcl
from pprint import pprint
from pythonlib import k8s_cluster, gitlab_mgr, kube_ctl, tiller_mgr


def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Configure GitLab with your AWS Cluster")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--pid', type=int, help='The ID of the project to link your cluster')
    group.add_argument('--gid', type=int, help='The ID of the group to link your cluster')
    parser.add_argument('--cluster_name', type=str, help='The k8s cluster-name to link')
    parser.add_argument('--provider', type=str, choices=['aws', 'gcp'], default="gcp", help="Which cloud provider are you using?")
    parser.add_argument('--env', type=str, help='The GitLab environment (instance) from your config (com or demo in mine)', default='demo')
    #parser.add_argument("--namespace", required=False, help='If you would like to define your namespace. Otherwise, it will be the cluster-name from your variables.')
    pargs = parser.parse_args(args)
    return pargs

def get_cluster_details(provider, cluster_name):

    if(provider == 'gcp'):
        gcp_client = container_v1.ClusterManagerClient()
        cluster_details = gcp_client.get_cluster(name=cluster_name)
        print(cluster_details)
    elif(provider == 'aws'):
        print(provider)
        eks_client = boto3.client('eks')
        cluster_details = eks_client.describe_cluster(name=cluster_name)
        api_server_endpoint = cluster_details['cluster']['endpoint']
        return api_server_endpoint
    else:
        raise ValueError("Invalid provider. Options are 'gcp' or 'aws'.")

def main(**kwargs):

    cluster_name = kwargs.get('cluster_name')
    pid = kwargs.get('pid')
    gid = kwargs.get('gid')
    env = kwargs.get('env')
    provider = kwargs.get('provider')
    myKubeCtl = kube_ctl.KubeCtl()


    # # https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html#existing-eks-cluster
    # # https://docs.gitlab.com/ee/user/project/clusters/add_gke_clusters.html
    # # 1.1 - Retrieve the certificate.
    default_secret_name, ca_token, ca_cert = myKubeCtl.get_token_n_cert(name_substr="default-token", namespace="default")
    # # 1.2 - Create admin token
    # # 1.2.1 file exists in cfg dir.
    # # 1.2.2 - Apply the service account to the cluster.
    myKubeCtl.apply_yaml(filename='cfg/k8s-admin-service-account.yaml')
    # # 1.2.3 file exists in cfg dir
    # # 1.2.4 - Apply the cluster role binding to the cluster.
    myKubeCtl.apply_yaml(filename='cfg/k8s-admin-cluster-role-binding.yaml')
    # 1.2.5 - Retrieve the token for the gitlab-admin service account.
    #    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
    kubesystem_secret_name, kubesystem_token, ks_cert = myKubeCtl.get_token_n_cert(name_substr="gitlab", namespace="kube-system")
    #kubesystem_secret_name, kubesystem_token, ks_cert = myKubeCtl.get_token_n_cert(user="gitlab-admin", provider=provider)
    # 1.3 - Locate the API server endpoint so GitLab can connect to the cluster

    #api_server_endpoint = "https://gitlab-s1p-apiserver-741541232.us-east-1.elb.amazonaws.com:6443"
    # WE NOW HAVE ALL OUR INFORMATION & CAN CONFIGURE GITLAB.

    k8scluster = k8s_cluster.K8sCluster(name=cluster_name, k8s_api_url=myKubeCtl.api_url, k8s_token=kubesystem_token, k8s_ca_cert=ca_cert)

    #get_cluster_details(provider, cluster_name)
    print("-"*30)
    pprint(k8scluster.__dict__)
    print("-"*30)

    glmgr = gitlab_mgr.GitLabMgr(env=env)

    # Send required json to GL to add the cluster.
    # if pid:
    #     glcluster = glcm.add_cluster_to_project(k8scluster=k8scluster, pid=pid)
    if gid:
       glcluster = glmgr.add_cluster_to_group(k8scluster=k8scluster, gid=gid)
    print("-"*30)
    pprint(glmgr.__dict__)
    print("-"*30)
    pprint(glcluster.__dict__)
    print("-"*30)
    # Now it's time to ensure appropriate software is installed.
    #tiller_install(namespace=tfcfg['gitlab_namespace']['default'])


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))