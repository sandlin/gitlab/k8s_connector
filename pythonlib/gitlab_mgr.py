import logging
import configparser
import os

from pathlib import Path
import gitlab
import pprint

logger = logging.getLogger(__name__)
class GitLabMgr(gitlab.Gitlab):

    def __init__(self, env):
        config = self.load_config(env)
        # Now let's start talking to GitLab
        self.gitlab = gitlab.Gitlab(config['url'], private_token=config['private_token'])

    def load_config(self, env):
        config = configparser.ConfigParser()
        config.read(os.path.join(str(Path.home()), '.python-gitlab.cfg'))

        if not config.has_section(env):
            raise configparser.NoSectionError("You are missing the {} section of your ~/.python-gitlab.cfg".format(env))
        try:
            return {'url': config.get(env, 'url'), 'private_token': config.get(env, 'private_token')}
        except Exception as e:
            raise e



    def add_cluster_to_project(self, k8scluster, pid):
        if not pid.isdigit():
            raise TypeError("pid must be an integer.")
        try:
            myproject = self.gitlab.projects.get(pid)
            # Is cluster already in the group?
            for c in myproject.clusters.list():
                if c.name == k8scluster.name:
                    print("Project Cluster {} already exists. It was created at {}".format(c.name, c.created_at))
                    return (c)
            cluster = myproject.clusters.create(k8scluster.__dict__)
            return (cluster)
        except gitlab.GitlabCreateError as e:
            logger.exception(e)
            raise (e)

    def add_cluster_to_group(self, k8scluster, gid):
        if not isinstance(gid, int):
            raise TypeError("gid must be an integer.")
        try:
            mygroup = self.gitlab.groups.get(gid)
            # Is cluster already in the group?
            for c in mygroup.clusters.list():
                if c.name == k8scluster.name:
                    print("Group Cluster {} already exists. It was created at {}".format(c.name, c.created_at))
                    return(c)
            cluster = mygroup.clusters.create(k8scluster.__dict__)
            return(cluster)
        except gitlab.GitlabCreateError as e:
            logger.exception(e)
            raise(e)
