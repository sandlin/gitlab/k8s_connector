
class K8sCluster():

    def __init__(self, name, k8s_api_url, k8s_token, k8s_ca_cert):
        self.name = name
        self.platform_kubernetes_attributes = {
            'api_url': k8s_api_url,
            'token': k8s_token,
            'ca_cert': k8s_ca_cert
        }
