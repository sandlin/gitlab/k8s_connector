import logging
import kubernetes
import re
import sys
import base64
from lib import kube_apply


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

class KubeCtl(object):

    def __init__(self):
        kubernetes.config.load_kube_config()
        self.k8sapi = kubernetes.client.CoreV1Api()
        self.api_url = self.k8sapi.api_client.configuration.host
    #     self.k8s_core_v1 = kubernetes.client.CoreV1Api()
    #     self.k8s_extensions_v1 = kubernetes.client.ExtensionsV1beta1Api()
    #     self.k8s_rbac_auth_v1 = kubernetes.client.RbacAuthorizationV1Api()


    def get_token_n_cert(self, name_substr: str, namespace: str) -> [str, str]:
        """
        Query the provided namespace for the secret matching the provided name substring. Retrieve its token.

        Translates to cmd:
            kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep k8s-admin | awk '{print $1}')

        :param name_substr: A substring of the service account for which you wish to retrieve the token.
        :type name_substr: str
        :param namespace: The namespace under which you wish to look for said account.
        :type namespace: str

        :raises: :class: `NameError`: The name_substr arg you provided is not found.

        :returns: Name of token matching name_substr && token associated with this name prefix.
        """
        secrets = self.k8sapi.list_namespaced_secret(namespace=namespace)
        for s in secrets.items:
            if re.match(name_substr, s.metadata.name):
                # We found the secret matching the name substring.
                decoded_cert = base64.b64decode(s.data['ca.crt']).decode('utf8')
                decoded_token = base64.b64decode(s.data['token']).decode('utf8')
                return s.metadata.name, decoded_token, decoded_cert
        logger.exception("Unable to find any secrets matching the substring {} in namespace {}".format(name_substr, namespace))
        raise NameError("Unable to find any secrets matching the substring {} in namespace {}".format(name_substr, namespace))

    #
    # def get_cluster_name():
    #     cm = K8sConfigMap.get_cm('kube-system')
    #     for item in cm.items:
    #         if 'kubeadm-config' in item.metadata.name:
    #             if 'clusterName' in item.data['ClusterConfiguration']:
    #                 cluster_name = re.search(r"clusterName: ([\s\S]+)controlPlaneEndpoint", \
    #                                          item.data['ClusterConfiguration']).group(1)
    #                 print("\nCluster name: {}".format(cluster_name))

    def get_ca_cert(self, namespace: str = "default") -> [str, str]:
        """https://docs.gitlab.com/ee/user/project/clusters/add_k8s_clusters.html#existing-k8s-cluster
        1.1 - Retrieve the certificate

        Translates to cmd:
            kubectl get secret $(kubectl get secrets | grep default-token | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

        :param namespace: The namespace from which to pull the cert
        :return: secret_name, secret_certificate
        note:: This is expecting a single secret to exist in the provided namespace.
        """
        secrets = self.k8sapi.list_namespaced_secret(namespace=namespace)
        if len(secrets.items) > 1:
            logger.warning(
                "You have {} secrets.\n   At this point there should only be 1.\n   If you have set things up manually / differently, this automation may fail.".format(
                    len(secrets.items)))
            sys.exit(1)
        mysecret = secrets.items[0]
        decoded_cert = base64.b64decode(mysecret.data['ca.crt']).decode('utf8')
        secret_name = mysecret.metadata.name
        return(secret_name, decoded_cert)

    def apply_yaml(self, filename: str):
        """kubectl apply -f <filename>

        :param filename: The yaml file you wish to apply via kubectl
        :type filename: str
        """
        try:
            with open(filename, 'r') as fp:
                kube_apply.fromYaml(fp)
        except FileNotFoundError as e:
            raise
        except UnboundLocalError as e:
            raise
        finally:
            fp.close()
