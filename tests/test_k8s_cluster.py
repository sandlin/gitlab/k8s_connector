import pytest
from pythonlib.k8s_cluster import K8sCluster

    #
    # def setUp(self):
    #     self.k8scluster = K8sCluster(name='testcluster', k8s_api_url='http://localhost', k8s_token='k8s_token_val', k8s_ca_cert='-----BEGIN CERTIFICATE-----\nfoo\nbar\n-----END CERTIFICATE-----\n')


def testInit():
    k8scluster = K8sCluster(name='testcluster', k8s_api_url='http://localhost', k8s_token='k8s_token_val',
                            k8s_ca_cert='-----BEGIN CERTIFICATE-----\nfoo\nbar\n-----END CERTIFICATE-----\n')
    assert isinstance(k8scluster, K8sCluster)
    # missing name arg
    with pytest.raises(TypeError):
        x = K8sCluster(k8s_api_url='ff', k8s_token=213, k8s_ca_cert=1234)

    # missing k8s_api_url arg
    with pytest.raises(TypeError):
        x = K8sCluster(name='ff', k8s_token=213, k8s_ca_cert=1234)

    # missing k8s_token arg
    with pytest.raises(TypeError):
        x = K8sCluster(name='ff', k8s_api_url=213, k8s_ca_cert=1234)

    # missing k8s_ca_cert arg
    with pytest.raises(TypeError):
        x = K8sCluster(name='ff', k8s_api_url=213, k8s_token=1234)

    # All args good
    x = K8sCluster(name='foo', k8s_api_url='xxx', k8s_token='xxx', k8s_ca_cert='asdf')
    assert isinstance(x, K8sCluster)
