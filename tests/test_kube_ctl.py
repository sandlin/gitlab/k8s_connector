import pytest
import pytest
#from pytest.mock import MagicMock, patch
from pythonlib.kube_ctl import KubeCtl



# # https://docs.gitlab.com/ee/user/project/clusters/add_k8s_clusters.html#existing-k8s-cluster
# # 1.1 - Retrieve the certificate.
@pytest.mark.skip(reason="missing test")
def test_retrieve_certificate():
    #default_secret_name, ca_token, ca_cert = myKubeCtl.get_token_n_cert(name_substr="default-token", namespace="default")
    pass

# 1.2.5 - Retrieve the token for the k8s-admin service account.
#    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep k8s-admin | awk '{print $1}')
@pytest.mark.skip(reason="missing test")
def test_retrieve_token_for_admin_account():
    # kubesystem_secret_name, kubesystem_token, ks_cert = get_token_n_cert(name_substr="k8s-admin", namespace="kube-system")
    pass
# # 1.2 - Create admin token
# # 1.2.1 file exists in cfg dir.
# # 1.2.2 - Apply the service account to the cluster.
@pytest.mark.skip(reason="missing test")
def test_apply_service_account_to_cluster():
    #myKubeCtl.apply_yaml(filename='cfg/k8s-admin-service-account.yaml')
    pass

# # 1.2.3 file exists in cfg dir
# # 1.2.4 - Apply the cluster role binding to the cluster.
@pytest.mark.skip(reason="missing test")
def test_apply_cluster_role_binding_to_cluster():
    #myKubeCtl.apply_yaml(filename='cfg/k8s-admin-cluster-role-binding.yaml')
    pass

@pytest.mark.skip(reason="missing test")
def test_get_token_n_cert():
    pass

@pytest.mark.skip(reason="missing test")
def test_get_ca_cert():
    pass

@pytest.mark.skip(reason="missing test")
def test_apply_yaml():

    # The real error here is FileNotFoundError, but we have a finally statement doing a fp.close, which kicks off
    # this UnboundLocalError
    with self.assertRaises(UnboundLocalError):
        self.kc.apply_yaml("thisfiledoesnotexist")

    print("TBD")
    pass
