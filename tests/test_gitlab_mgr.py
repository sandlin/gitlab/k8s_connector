import pytest
from pytest_mock import mocker
#from pytest_mock import mock
import configparser
from pythonlib.gitlab_mgr import GitLabMgr

def test_load_config(mocker):

    env = 'demo'

    # Test mocking config values
    mocker.patch.object(configparser.ConfigParser, 'has_section', return_value=True)
    mocker.patch.object(configparser.ConfigParser, 'get')
    glmgr = GitLabMgr(env=env)
    assert isinstance(glmgr, GitLabMgr)

    # Test section missing
    mocker.patch.object(configparser.ConfigParser, 'has_section', return_value=False)
    with pytest.raises(configparser.NoSectionError,
                       match="You are missing the {} section of your ~/.python-gitlab.cfg".format(env)):
        glmgr = GitLabMgr(env=env)



def test_add_cluster_bad_args(mocker):
    mock_cfg = {'url': 'https://gitlab-core.us.gitlabdemo.cloud/', 'private_token': 'foobar'}
    mocker.patch.object(GitLabMgr, 'load_config', return_value=mock_cfg)
    glmgr = GitLabMgr(env='prod')

    # No pid or gid
    with pytest.raises(TypeError):
        glmgr.add_cluster_to_group()

    # No pid or gid
    with pytest.raises(TypeError):
        glmgr.add_cluster_to_project()

    with pytest.raises(TypeError):
        glmgr.add_cluster_to_project("x")

#
# @pytest.mark.skip(reason="missing test")
# def test_add_cluster_to_group():
#     pass
#
# @pytest.mark.skip(reason="missing test")
# def test_group_already_has_cluster():
#     pass
#
# @pytest.mark.skip(reason="missing test")
# def test_project_already_has_cluster():
#     pass
#
# @pytest.mark.skip(reason="missing test")
# def test_project_group_id_invalid():
#     pass
#
# @pytest.mark.skip(reason="missing test")
# def test_success():
#     pass

    ###
    # These tests need added:
    # Project already has cluster
    # Group already has cluster
    # Successful add to Project
    # Successful add to Group
    ###

