
---

[[_TOC_]]

---

## Purpose
This project reads the configuration from your local Kubectl & configures GitLab to connect.

---

## System Setup
> NOTE: The instructions below are focused on MacOS.

#### GitLab OAuth Token

1. Setup your auth token @ https://gitlab.com/profile/personal_access_tokens
   1. Grant your token the following Scopes: `api`
   1. Copy the token generated as it will not be accessible once you leave the page
1. Create your python-gitlab auth / config file `~/.python-gitlab.cfg`
   1. Replace the private_token value with the value received above.
   1. Your file will look like:
        ```shell script
        # REF: https://python-gitlab.readthedocs.io/en/stable/cli.html#configuration
        [global]
        default = com
        ssl_verify = true
        timeout = 5

        [com]
        url = https://gitlab.com
        private_token = xxxxxxxxxxx
        api_version = 4
        ```

#### Python Environment

- **Note:** This repo uses Pipenv but you can use what you want for dependency management.
- **Note:** This instruction set is written for MacOS. If you are on Linux, the only items which will differ is how you install `pipenv` and the location of your home directory.

1. Ensure you have `Python 3.7` installed
    ```shell script
    ➜ python3 --version
    Python 3.7.7
    ```
1. Install `pipenv`.
    ```
    ➜ brew install pipenv
   ...
    ```
1. Make certain you are in the `gitlab_api` clone directory.
    ```shell script
    ➜ pwd
    /Users/hulko/workspaces/gitlab_api
    ```
1. Initialize your `pipenv`
    ```shell script
    ➜ pipenv install
    Creating a virtualenv for this project…
    ...
    To activate this project's virtualenv, run pipenv shell.
    Alternatively, run a command inside the virtualenv with pipenv run.
    ```
1. Activate your project's virtualenv
    ```shell script
    ➜ pipenv shell
    Launching subshell in virtual environment…
    . /Users/hulko/.local/share/virtualenvs/gitlab_api-Q0PixHAH/bin/activate
    ```
   
---

## Prior to execution
1. You have create your cluster in AWS or GC.
1. You have your credentials configured.
1. You have kubectl configured & pointing to the correct context.

---

## Args
You must include either `pid` or `gid`. `recursive` is optional
- `--pid <YOUR PROJECT ID>`: The project ID you would like to query.
- `--gid <YOUR GROUP ID>`: The group ID you would like to query.
- `--env`: The GitLab Instance (from your config to in which this PID or GID is located)
- `--cluster_name`: The K8S cluster to which you are linking GitLab.

---
## Example
```
python gl_add_cluster.py --gid 3 --cluster_name demosys-us-rkrishna-cluster --env prod
```